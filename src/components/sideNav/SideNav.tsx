import React, { Fragment } from 'react';
import './SideNav.less';
import Icon from '@mdi/react';
import { RouteComponentProps, withRouter } from 'react-router';
import { Popover } from 'antd';
import { mdiMenuRight } from '@mdi/js';

export interface ISideNavRoute {
    path: string | string[];
    exact: boolean;
    component: any;
    icon?: string;
    label: string;
    children?: ISideNavRoute[];
}

export interface ISideNavProps {
    routes: ISideNavRoute[];
}

const SideNav: React.FunctionComponent<ISideNavProps & RouteComponentProps> = (props) => {
    // console.log(props.history.location);

    /**
     * Determine whether the current path in history is equal or contain in the given paths
     * @param path Route path
     * @returns {boolean}
     */
    const isCurrentPath = (path: string | string[]) => {
        var currentPath = props.history.location.pathname;

        if (path instanceof Array) {
            return path.indexOf(currentPath) >= 0;
        } else {
            return path === currentPath;
        }
    }

    /**
     * Side nav item click event handler
     * @param e Mouse click event
     * @param path 
     */
    const sideNavItemClick = (e: React.MouseEvent<HTMLElement>, path: string | string[]) => {
        if (path instanceof Array) {
            props.history.push(path[0]);
        } else {
            props.history.push(path);
        }
    }

    /**
     * Render menu item
     * @param route Route information
     * @param idx 
     */
    const renderMenuItem = (route: ISideNavRoute, idx: number) => {
        return (
            <div className={"side-nav-item " + (isCurrentPath(route.path) ? "selected" : "")} onClick={(e) => sideNavItemClick(e, route.path)} key={idx}>
                {route.icon !== undefined ?
                <Icon path={route.icon} size="30" /> : null}
                <div>
                    {route.label}
                </div>
                {route.children !== undefined ? 
                <Icon path={mdiMenuRight} size="20" className="submenu-icon" />
                : null }
            </div>

        );
    }

    /**
     * Render popover content
     * @param routes List is popover items
     */
    const renderPopoverContent = (routes: ISideNavRoute[]) => {
        return (
            <Fragment>
                {routes.map((route: ISideNavRoute, idx: number) => {
                    return (
                        <div className="submenu-nav-item" onClick={(e) => sideNavItemClick(e, route.path)} key={idx}>
                            {route.label}
                        </div>
                    );
                })}
            </Fragment>
        );
    }

    /**
     * Render menu item with popover
     * @param {ISideNavRoute} route Route information
     * @param {number} idx 
     */
    const renderMenuItemWithPopover = (route: ISideNavRoute, idx: number) => {
        return (
            <Popover content={renderPopoverContent(route.children || [])} placement="rightTop" key={idx}>
                {renderMenuItem(route, idx)}
            </Popover>
        );
    }

    return (
        <div className="side-nav">
            {props.routes.map((route, idx) => {

                if (route.children !== undefined) {
                    return renderMenuItemWithPopover(route, idx);
                } else {
                    return renderMenuItem(route, idx);
                }
            })}
        </div>

    );
}

export default withRouter(SideNav);