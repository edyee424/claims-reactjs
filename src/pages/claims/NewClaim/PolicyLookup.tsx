import React, { Fragment, useEffect, useState, useContext } from 'react';
import { Form, Radio, Tooltip, Select, Input, Button, Space, Table, Row, Col, DatePicker } from 'antd';
import { RadioChangeEvent } from 'antd/lib/radio';
import { RangeValue } from 'rc-picker/lib/interface';
import * as faker from 'faker';
import moment, { Moment } from 'moment';
import Icon from '@mdi/react';
import { mdiInformation, mdiMenuUp, mdiMenuDown } from '@mdi/js';
import { IPolicySearchResult, INewClaimStepProps } from './NewClaimInterfaces';
import { NewClaimContext } from './NewClaimProvider';
import { withRouter, RouteComponentProps } from 'react-router';
const { Option } = Select;
const { RangePicker } = DatePicker;

export interface IPolicyLookupProps {

}

const PolicyLookup: React.FunctionComponent<INewClaimStepProps & RouteComponentProps> = (props) => {
    var context = useContext(NewClaimContext);

    const [searchBy, setSearchBy] = useState('policy');
    const [policyNumber, setPolicyNumber] = useState<string | null>(null);
    const [clientCompanyNumber, setClientCompanyNumber] = useState<any | null>(null);
    const [fileNumber, setFileNumber] = useState<any | null>(null);
    const [dateOfLoss, setDateOfLoss] = useState<string | null>(null);
    const [insuredCompany, setInsuredCompany] = useState<string | null>(null);

    const [mainForm] = Form.useForm();
    const [searchResultsForm] = Form.useForm();

    const [showSearchResult, setShowSearchResult] = useState(false);
    const [policySearchResults, setPolicySearchResults] = useState<IPolicySearchResult[]>([]);
    const [filteredPolicySearchResults, setFilteredPolicySearchResults ] = useState<IPolicySearchResult[]>([]);

    const [policySearchResultsHeaders, setPolicySearchResultsHeaders] = useState<any>();
    const [expandIconColumnIndex, setExpandIconColumnIndex] = useState(8);

    const [policyNumberFilter, setPolicyNumberFilter] = useState<string | null>(null);
    const [trnxNumberFilter, setTrnxNumberFilter] = useState<string | null>(null);
    const [verNumberFilter, setVerNumberFilter] = useState<string | null>(null);
    const [productFilter, setProductFilter] = useState<string | null>(null);
    const [premiumFilter, setPremiumFilter] = useState<string | null>(null);
    const [insuredDateFilter, setInsuredDateFilter] = useState<RangeValue<moment.Moment>>();
    const [effDateFilter, setEffDateFilter] = useState<RangeValue<moment.Moment>>();
    const [expDateFilter, setExpDateFilter] = useState<RangeValue<moment.Moment>>();

    const [showContinueBtn, setShowContinueBtn] = useState(false);

    const searchByOptions = [
        { label: 'Policy Number', value: 'policy' },
        { label: 'File Number', value: 'file' }
    ]

    interface IOptionType {
        value: string;
        label: string;
    }

    const clientCompanies: IOptionType[] = [
        { value: '1000', label: 'Company A' },
        { value: '1001', label: 'Company B' },
        { value: '1002', label: 'Company C' },
        { value: '1003', label: 'Company D' },
        { value: '1004', label: 'Company E' },
        { value: '1005', label: 'Company F' },
        { value: '1006', label: 'National Aeronautics and Space Administration' },
    ]

    const baseSearchResultGridHeader: any[] = [
        {
            title: 'Transaction Number', /*dataIndex: 'transactionNumber',*/ key: 'transactionNumber', className: "data-cell small-number trans-number",
            children: [
                {
                    title: <Input type="text" className="table-header-input small-number" allowClear 
                        onChange={(e) => { 
                            console.log(`Set transaction number filter: ${e.target.value}`)
                            setTrnxNumberFilter(e.target.value); 
                        }}></Input>,
                    dataIndex: 'transactionNumber', key: 'transactionNumber',
                    className: "data-cell center"
                }
            ]
        },
        {
            title: 'Version Number', /*dataIndex: 'versionNumber',*/ key: 'versionNumber', className: "data-cell small-number ver-number", sorter: (a: IPolicySearchResult, b: IPolicySearchResult, sortOrder: string) => { 
                return a.versionNumber - b.versionNumber;
            },
            children: [
                {
                    title: <Input type="text" className="table-header-input small-number" allowClear 
                        onChange={(e) => { setVerNumberFilter(e.target.value); }}></Input>,
                    dataIndex: 'versionNumber', key: 'versionNumber',
                    className: "data-cell center"
                }
            ]
        },
        {
            title: 'Product', /*dataIndex: 'product',*/ key: 'product', className: "data-cell product", sorter: (a: IPolicySearchResult, b: IPolicySearchResult, sortOrder: string) => {
                if (a.product < b.product) return -1;
                if (a.product > b.product) return 1;
                return 0;
            }, 
            children: [
                {
                    title: <Input type="text" className="table-header-input" allowClear 
                        onChange={(e) => { setProductFilter(e.target.value); }}></Input>, 
                    dataIndex: 'product', key: 'product', className: "data-cell"
                }
            ]
        },
        {
            title: 'Premium', /*dataIndex: 'premium',*/ key: 'premium', className: "data-cell small-number premium", sorter: (a: IPolicySearchResult, b: IPolicySearchResult, sortOrder: string) => {
                return a.premium < b.premium;
            },
            children: [
                {
                    title: <Input type="text" className="table-header-input" allowClear 
                        onChange={(e) => { setPremiumFilter(e.target.value); }}></Input>, 
                    dataIndex: 'premium', key: 'premium', className: "data-cell small-number center"
                }
            ]
        },
        {
            title: "Insured Date", /*dataIndex: 'insuredDate', key: 'insuredDate',*/ className: "data-cell insured-date",
            children: [
                {
                    title: <RangePicker placeholder={['From', 'To']} onCalendarChange={(values: RangeValue<moment.Moment>) => { 
                        setInsuredDateFilter(values);
                    }} />,
                    dataIndex: 'insuredDate', key: 'insuredDate', className: "data-cell insured-date",
                    render: (insuredDate: any, record: any) => { return insuredDate.format('DD MMM YYYY'); }
                }
            ]
        },
        {
            title: 'Effective Date', /*dataIndex: 'effectiveDate', key: 'effectiveDate',*/ className: "data-cell eff-date",
            children: [
                {
                    title: <RangePicker placeholder={['From', 'To']} onCalendarChange={(values: RangeValue<moment.Moment>) => { 
                        setEffDateFilter(values);
                    }} />, 
                    dataIndex: 'effectiveDate', key: 'effectiveDate', className: "data-cell eff-date",
                    render: (effectiveDate: any, record: any) => { return effectiveDate.format('DD MMM YYYY'); }
                }
            ]
        },      
        {
            title: 'Expiry Date', /*dataIndex: 'expiryDate',*/ key: 'expiryDate', className: "data-cell exp-date",
            children: [
                {
                    title: <RangePicker placeholder={['From', 'To']} onCalendarChange={(values: RangeValue<moment.Moment>) => { 
                        setExpDateFilter(values);
                    }} />,
                    dataIndex: 'expiryDate', key: 'expiryDate', className: "data-cell exp-date",
                    render: (expiryDate: any, record: any) => { return expiryDate.format('DD MMM YYYY'); }
                }
            ]
        }               
    ];

    /**
     * The Search By segmented buttons change event handler
     */
    const changeSearchBy = (e: RadioChangeEvent) => {
        // console.log(e.target.value);
        setSearchBy(e.target.value);

        // Clear form fields
        mainForm.resetFields();

        // Clear component states
        setPolicyNumber(null);
        setFileNumber(null);
        setDateOfLoss(null);
        setClientCompanyNumber(null);
        setInsuredCompany(null);

        // Clear search results and hide search results
        setShowSearchResult(false);
        setPolicySearchResults([])

        // Set search result columns
        if (e.target.value === "policy") {
            setPolicySearchResultsHeaders(baseSearchResultGridHeader);
            setExpandIconColumnIndex(8);
        } else {
            var headers = [{
                title: 'Policy Number', /*dataIndex: 'policyNumber',*/ key: 'policyNumber', className: 'data-cell small-number policy-number',
                children: [
                    {
                        title: <Input type="text" className="table-header-input small-number" allowClear 
                            onChange={(e) => { setPolicyNumberFilter(e.target.value); }} />,
                        dataIndex: 'policyNumber', key: 'policyNumber',
                        className: 'data-cell center'
                    }
                ]
            }].concat(baseSearchResultGridHeader);

            console.log(headers);
            setPolicySearchResultsHeaders(headers);
            setExpandIconColumnIndex(9);
        }
    }

    /**
     * Policy lookup form completion. Generates dummy data.
     * @param {any} values Form values
     */
    const policyLookupOnFinish = (values: any) => {
        console.log(values);

        // TODO: Query the backend

        var fakeData = [];
        var numberOfRec = faker.random.number({ min: 20, max: 100 });
        for (var i = 1; i <= numberOfRec; i++) {
            fakeData.push({
                key: i, // This is the (DOM) row key
                policyNumber: faker.random.number({ min: 10000, max: 99999 }),
                transactionNumber: i,
                versionNumber: faker.random.number({ min: 1, max: 20 }),
                product: `${faker.random.number({ min: 1000, max: 9999 })}-${faker.random.word()}`,
                premium: `${faker.random.number({ min: 1, max: 100 })}%`,
                insuredDate: moment(faker.date.past(10)),
                effectiveDate: moment(faker.date.past(9)),
                expiryDate: moment(faker.date.past(8)),
                details: {
                    trxStartDate: moment(faker.date.past(10)),
                    trxEndDate: moment(faker.date.past(9)),
                    businessCode: faker.random.number({ min: 1000, max: 9999 }),
                    writingOffice: faker.random.arrayElement(['Toronto', 'Montreal', 'Calgary', 'Hamilton', 'Halifax', 'Vancouver']),
                    broker: {
                        street: faker.address.streetAddress(),
                        city: faker.address.city(),
                        provCode: faker.address.stateAbbr(),
                        postal: faker.address.zipCode(),
                        phone: faker.phone.phoneNumber()
                    }
                }
            })
        }

        if (searchBy === "policy") {
            setFileNumber(faker.random.number({ min: 10000, max: 99999 }));
        }

        setInsuredCompany(faker.company.companyName());

        if (policySearchResultsHeaders === undefined) {
            setPolicySearchResultsHeaders(baseSearchResultGridHeader);
        }

        setPolicySearchResults(fakeData);
        setFilteredPolicySearchResults(fakeData);
        setShowSearchResult(true);
    }

    /**
     * Search results radio button selection handler
     * @param {IPolicySearchResult} policy Selected policy
     */
    const selectPolicy = (policy: IPolicySearchResult) => {
        // console.log(`In selectPolicy:`, policy)
        setShowContinueBtn(true);

        context.setPolicy({
            claimNumber: faker.random.number({ min: 1000000, max: 9999999 }),
            insured: insuredCompany,
            policyNumber: policy.policyNumber,
            fileNumber: fileNumber,
            dateOfLoss: dateOfLoss
        })
    }

    /**
     * Proceed to loss location
     * @param {any} values Form values
     */
    const proceedToLossLocation = (values: any) => {
        if (props.nextStep) {
            props.nextStep();
        }
    }

    useEffect(() => {
        var filteredData = policySearchResults.filter((rec) => {
            if (policyNumberFilter && policyNumberFilter.length > 0) {
                return (rec.policyNumber.toString() === policyNumberFilter);
            } else {
                return true;
            } 
        }).filter((rec) => {
            if (trnxNumberFilter && trnxNumberFilter.length > 0) {
                return (rec.transactionNumber.toString() === trnxNumberFilter);
            } else {
                return true;
            }
        }).filter((rec) => {
            if (verNumberFilter && verNumberFilter.length > 0) {
                return (rec.versionNumber.toString() === verNumberFilter);
            } else {
                return true;
            }
        }).filter((rec) => {
            if (productFilter && productFilter.length > 0) {
                return (rec.product.indexOf(productFilter) >= 0);
            } else {
                return true;
            }
        }).filter((rec) => {
            if (premiumFilter && premiumFilter.length > 0) {
                return (rec.premium.toString() === premiumFilter);
            } else {
                return true;
            }
        }).filter((rec) => {
            if (insuredDateFilter && insuredDateFilter.length === 2) {
                return (rec.insuredDate.isBetween(insuredDateFilter[0], insuredDateFilter[1]));
            } else {
                return true;
            }
        }).filter((rec) => {
            if (effDateFilter && effDateFilter.length === 2) {
                return (rec.effectiveDate.isBetween(effDateFilter[0], effDateFilter[1]));
            } else {
                return true;
            }
        }).filter((rec) => {
            if (expDateFilter && expDateFilter.length === 2) {
                return (rec.expiryDate.isBetween(expDateFilter[0], expDateFilter[1]));
            } else {
                return true;
            }
        })

        setFilteredPolicySearchResults(filteredData);        
    }, [policyNumberFilter, trnxNumberFilter, verNumberFilter, productFilter, premiumFilter, insuredDateFilter, effDateFilter, expDateFilter])

    return (
        <Fragment>
            <div>To create the claim, start with policy lookup placeholder for text.</div>

            <Form layout="vertical" className="claim-search-form" name="policyLookup" form={mainForm} onFinish={policyLookupOnFinish}>
                <Form.Item>
                    <div>Search By</div>
                    <Radio.Group options={searchByOptions} value={searchBy} buttonStyle="solid" optionType="button" onChange={changeSearchBy} />
                </Form.Item>
    
                {searchBy === 'policy' ?
                <Fragment>
                    <div>Policy Number</div>
                    <Form.Item name="policyNumber" rules={[{ required: true, message: 'Please enter a Policy Number' }]}>
                        <Input onChange={(e) => { setPolicyNumber(e.target.value); }} />
                    </Form.Item>
        
                    <div className="flex align-middle space-between-5">
                        <span>Client Company Number</span>
                        <Tooltip title="Something about client company number" className="ml-sm" placement="topLeft">
                            <Icon path={mdiInformation} size="18" className="highlight" />
                        </Tooltip>
                    </div>
                    <Form.Item name="clientCompanyNumber" rules={[{ required: true, message: 'Please select a Client Company Number'}]}>
                        <Select showSearch dropdownMatchSelectWidth={true} 
                                placeholder="--- Select ---"
                                filterOption={(input, option) => {
                                    if (option && option.children) {
                                        return option?.children.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0 || false;
                                    }
                                    return false;
                                }} 
                                onChange={(value, option) => { setClientCompanyNumber(value); }}>
                            {clientCompanies.map((company, idx) => {
                                return (
                                <Option value={company.value} key={idx}>({company.value}) {company.label}</Option>
                                );
                            })}
                        </Select>
                    </Form.Item>
                </Fragment> : 
                <Fragment>
                    <div>File Number</div>
                    <Form.Item name="fileNumber" rules={[{ required: true, message: 'Please enter a File Number' }]}>
                        <Input onChange={(e) => { setFileNumber(e.target.value); }} />
                    </Form.Item>
                </Fragment>
                }
        
                <div>Date of Loss</div>
                <Form.Item name="dateOfLoss" rules={[{ required: true, message: 'Please enter a Date of Loss' }]}>
                    <Input type="date" onChange={(e) => { setDateOfLoss(e.target.value); }} />
                </Form.Item>
        
                <Form.Item>
                    <Button type="primary" htmlType="submit">Lookup</Button>
                </Form.Item>
            </Form>

            {showSearchResult ? 
                <Fragment>
                    <h2 className='feature-title'>Policy Results</h2>
                    <Space direction="vertical" size="middle" style={{ width: "100%" }}>
                        <div>To create the claim, start with policy lookup placeholder for text.</div>
                        <div>
                            <Row className="policy-data-block">
                                <Col span="6">
                                    <div>Insured</div>
                                    <div className="policy-data">{insuredCompany}</div>
                                </Col>
                                {searchBy === "policy" ?
                                <Col span="6">
                                    <div>Policy Number</div>
                                    <div className="policy-data">{policyNumber}</div>
                                </Col>
                                : null }
                                <Col span="6">
                                    <div>File Number</div>
                                    <div className="policy-data">{fileNumber}</div>
                                </Col>
                                <Col span="6">
                                    <div>Date of Loss</div>
                                    <div className="policy-data">{moment(dateOfLoss).format('DD MMM YYYY')}</div>
                                </Col>
                            </Row>
                        </div>
                        <Form form={searchResultsForm} onFinish={proceedToLossLocation}>
                            <Table className="search-results" 
                                size="small" 
                                dataSource={filteredPolicySearchResults} 
                                columns={policySearchResultsHeaders} 
                                pagination={{
                                    position: ['bottomLeft'],
                                    showTotal: (total: number, range: [number, number]) => {
                                        return (<span>{range[0]}-{range[1]} of {total} items</span>);
                                    },
                                    itemRender: (current, type, originalElement) => {
                                        return originalElement;
                                    }
                                }} expandable={{
                                    expandIconColumnIndex: expandIconColumnIndex,
                                    expandIcon: ({ expanded, onExpand, record }) => {
                                        return (<div onClick={(e) => onExpand(record, e)} className="data-cell no-wrap expand show-details">
                                            <span>Other Details</span>
                                            <Icon path={expanded ? mdiMenuUp : mdiMenuDown } size="1rem" />
                                        </div>)
                                    },
                                    expandedRowRender: (record: any) => {
                                        console.log(record);
                                        return (
                                            <div className="policy-details data-cell">
                                                <Row>
                                                    <Col span={12}>
                                                        <Row>
                                                            <Col span={8}>Transaction Period</Col>
                                                            <Col span={16} className="bold">
                                                                {moment(record.details.trxStartDate).format('DD MMM YYYY')} to {moment(record.details.trxEndDate).format('DD MMM YYYY')}
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col span={8}>Business Code</Col>
                                                            <Col span={16} className="bold">{record.details.businessCode}</Col>
                                                        </Row>
                                                        <Row>
                                                            <Col span={8}>Writing Office</Col>
                                                            <Col span={16} className="bold">{record.details.writingOffice}</Col>
                                                        </Row>
                                                    </Col>
                                                    <Col span={12}>
                                                        <Row>
                                                            <Col span={8}>Broker Address</Col>
                                                            <Col span={16} className="bold">
                                                                {record.details.broker.street}<br />
                                                                {record.details.broker.city}, {record.details.broker.provCode} {record.details.broker.postal}
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col span={8}>Broker Phone</Col>
                                                            <Col span={16} className="bold">{record.details.broker.phone}</Col>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                            </div>
                                        );
                                    }
                                }} 
                                rowSelection={{ type: 'radio', onChange: (key, row) => { selectPolicy(row[0]); }}}>
    
                            </Table>

                            {showContinueBtn ?
                            <Form.Item>
                                <Button type="primary" htmlType="submit">Continue</Button>
                            </Form.Item>
                            : null}
                        </Form>
                    </Space>
                </Fragment>

            : null }

        </Fragment>
    );
}

export default withRouter(PolicyLookup);