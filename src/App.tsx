import React, { Fragment } from 'react';
// import logo from './logo.svg';
import './App.less';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import { mdiMonitorDashboard, mdiFileDocumentOutline, mdiClipboardEditOutline, 
      mdiCurrencyUsdCircleOutline, mdiAccountMultiple, mdiShieldCheckOutline } from '@mdi/js';

import { ISideNavRoute } from './components/sideNav/SideNav';

import Claims from './pages/claims/Claims';
import Dashboard from './pages/dashboard/Dashboard';
import InvestigationNotes from './pages/investigationNotes/InvestigationNotes';
import Payments from './pages/payments/Payments';
import Vendors from './pages/vendors/Vendors';
import Authorization from './pages/authorization/Authorization';
import SideNav from './components/sideNav/SideNav';
import TopNav from './components/header/TopNav';

const { Header, Sider, Content } = Layout;

function App() {
  var contentRoutes: ISideNavRoute[] = [
    { path: ["/dashboard", "/"], exact: true, component: () => <Dashboard />, icon: mdiMonitorDashboard, label: "Dashboard" },
    { path: ["/claims", "/claims/newClaim", "/claims/newClaim", "/claims/viewClaim"], exact: true, component: () => <Claims />, icon: mdiFileDocumentOutline, label: "Claims", children: [
      { path: "/claims/newClaim", exact: true, component: () => <Claims />, label: "New Claim" },
      { path: "/claims/viewClaim", exact: true, component: () => <Claims />, label: "View Claim" }
    ] },
    { path: "/investigation", exact: true, component: () => <InvestigationNotes />, icon: mdiClipboardEditOutline, label: "Investigation Notes" },
    { path: "/payments", exact: true, component: () => <Payments />, icon: mdiCurrencyUsdCircleOutline, label: "Reserves & Payments" },
    { path: "/vendors", exact: true, component: () => <Vendors />, icon: mdiAccountMultiple, label: "Vendors" },
    { path: "/authorization", exact: true, component: () => <Authorization />, icon: mdiShieldCheckOutline, label: "Authorization" },
  ]

  return (
    <Router>
      <Layout className="full-height">
        <Header>
          <TopNav />
        </Header>
        <Layout>
          <Sider width="100">
            <SideNav routes={contentRoutes} />
          </Sider>
          <Content className="overflow">
            <Switch>
              {contentRoutes.map((r, idx) => (
                <Route exact={r.exact} path={r.path} render={r.component} key={idx} />
              ))}
            </Switch>
          </Content>
        </Layout>
      </Layout>
    </Router>
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.tsx</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
