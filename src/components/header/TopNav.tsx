import React from 'react';
import { Row, Col, Divider } from 'antd';

import './TopNav.less';

export interface ITopNavProps {
    
}

const TopNav = (props: any) => {
    return (
        <Row className="top-nav">
            <Col span="12" className="title-block">
                <span className="logo">CLx</span> 
                <Divider type="vertical" className="title-divider" /> 
                <span className="app-title">Claims Experience</span>
            </Col>
            <Col span="12" className="right-justify">Top Nav Buttons</Col>
        </Row>
    );
}

export default TopNav;