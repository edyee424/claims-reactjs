import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import NewClaim from './NewClaim/NewClaim';
import ViewClaim from './ViewClaim';

export interface IClaimsProps {

}

const Claims: React.FunctionComponent<IClaimsProps & RouteComponentProps> = (props) => {
    const isNewClaimStep = (currentPath: string): boolean => {
        const paths = [
            '/claims/newClaim'
        ];

        return paths.indexOf(currentPath) >= 0;
    }

    const isViewClaimStep = (currentPath: string): boolean => {
        const paths: string[] = [
            // TODO: Add paths
        ];

        return paths.indexOf(currentPath) >= 0;
    }


    return (
        <Fragment>
            {isNewClaimStep(props.history.location.pathname) ? <NewClaim /> : null }
            {isViewClaimStep(props.history.location.pathname) ? <ViewClaim /> : null }
        </Fragment>
    );
}

export default withRouter(Claims);