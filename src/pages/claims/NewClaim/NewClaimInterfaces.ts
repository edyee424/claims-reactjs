import moment, { Moment } from 'moment';

export interface IGenericAddress {
    street: string;
    city: string;
    provCode: string;
    postal: string;
}

// These are policy lookup interfaces

export interface IBrokerAddress extends IGenericAddress {
    phone: string;
}

export interface IPolicyDetails {
    trxStartDate: Moment;
    trxEndDate: Moment;
    businessCode: number;
    writingOffice: string;
    broker: IBrokerAddress;
}

export interface IPolicySearchResult {
    key: number;
    policyNumber: number;
    transactionNumber: number;
    versionNumber: number;
    product: string;
    premium: string;
    insuredDate: Moment;
    effectiveDate: Moment;
    expiryDate: Moment;
    details: IPolicyDetails;       
}

// This loss location grid data interface

export interface ILossLocations {
    clientName: string;
    status: number;
    locationNumber: number;
    address: IGenericAddress;
}

// These interfaces are for data stored in the NewClaimContext

export interface ISelectedPolicy {
    claimNumber?: number;
    insuredCompany?: string;
    policyNumber?: number;
    fileNumber?: number;
    dateOfLoss?: Moment;
}

export interface ISelectedLocation {

}

// New claim step component properties 

export interface INewClaimStepProps {
    lastStep?(): void;
    nextStep?(): void;
}
