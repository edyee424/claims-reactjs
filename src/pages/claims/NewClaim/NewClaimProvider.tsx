import React, { useState } from 'react';
import { ISelectedPolicy, ISelectedLocation } from './NewClaimInterfaces';

export const NewClaimContext = React.createContext<any>({});

type Props = {
    children: React.ReactNode;
}

const NewClaimProvider = ({children}: Props) => {
    const [ policy, setPolicy ] = useState<ISelectedPolicy | null>();
    const [ location, setLocation ] = useState<ISelectedLocation | null>();

    return (
        <NewClaimContext.Provider value={{policy, setPolicy, location, setLocation }}>
            {children}
        </NewClaimContext.Provider>
    );
}

export default NewClaimProvider;