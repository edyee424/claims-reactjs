import React, { useState, useContext } from 'react';
import { Row, Col } from 'antd';
import { withRouter, RouteComponentProps } from 'react-router';
import moment, { Moment } from 'moment';

import Steps from '../../../components/steps/Steps';
import PolicyLookup from './PolicyLookup';
import NewClaimProvider, { NewClaimContext } from './NewClaimProvider';
import LossLocation from './LossLocation';

import '../Claims.less';

const SelectedPolicy = (props: any) => {
    var context = useContext(NewClaimContext);

    return (
        <Row>
            <Col span="4">
                <div>Claim Number</div>
                <div className="policy-data">{context.policy?.claimNumber}</div>
            </Col>
            <Col span="4">
                <div>Insured</div>
                {console.log(context)}
                <div className="policy-data">{context.policy?.insured}</div>
            </Col>
            <Col span="4">
                <div>Policy Number</div>
                <div className="policy-data">{context.policy?.policyNumber}</div>
            </Col>
            <Col span="4">
                <div>File Number</div>
                <div className="policy-data">{context.policy?.fileNumber}</div>
            </Col>
            <Col span="4">
                <div>Date of Loss</div>
                <div className="policy-data">{moment(context.policy?.dateOfLoss).format("DD MMM YYYY")}</div>

            </Col>
        </Row>

    );
}

const NewClaim: React.FunctionComponent<RouteComponentProps> = (props) => {
    var context = useContext(NewClaimContext);
    // var insured = context.policy?.insured;
    const [insured, setInsured] = useState(context.policy?.insured);

    const [currentStep, setCurrentStep] = useState<number>(0);

    const nextStep = () => {
        var currStep = currentStep;
        setCurrentStep(++currStep);
    }

    const lastStep = () => {
        setCurrentStep(currentStep - 1);
    }

    const steps = [
        { label: 'Policy Lookup' },
        { label: 'Loss Location' },
        { label: 'Coverage' },
        { label: 'Additional Information' },
        { label: 'Agent Assignment' },
        { label: 'Review and Save' }
    ]

    return (
        <NewClaimProvider>
            <div className="content-block">
                <h1 className='feature-title'>Create New Claim</h1>
                {currentStep > 0 ?
                <SelectedPolicy />
                : null }
            </div>
            <Steps current={currentStep} steps={steps} />
            <div className="content-block">

                {currentStep === 0 ? <PolicyLookup nextStep={nextStep} /> : null}
                {currentStep === 1 ? <LossLocation nextStep={nextStep} lastStep={lastStep} /> : null}
            </div>
        </NewClaimProvider>
    );
}

export default withRouter(NewClaim);