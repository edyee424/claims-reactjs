import React from 'react';
import Icon from '@mdi/react';
import './Steps.less';

export interface IStep {
    icon?: string;
    label: string;
}

export interface IStepsProps {
    current: number;
    steps: IStep[];
}

const Steps = (props: IStepsProps) => {
    return (
        <div className="steps">
            {props.steps.map((step, idx) => {
                return (
                    <span className={"step " + (props.current === idx ? "selected" : "")} key={idx}>
                        {step.icon !== undefined ? <Icon path={step.icon} size="20" /> : null }
                        {step.label}
                    </span>
                )
            })}
        </div>
    );
}

export default Steps;
