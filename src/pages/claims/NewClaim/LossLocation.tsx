import React, { Fragment, useEffect, useState, useContext } from 'react';
import { Space, Form, Table, Button } from 'antd';
import { ColumnsType } from 'antd/lib/table'
import { INewClaimStepProps, ILossLocations } from './NewClaimInterfaces';
import { mdiCheckCircle, mdiCloseCircle } from '@mdi/js';
import Icon from '@mdi/react';
import * as faker from 'faker';

import { NewClaimContext } from './NewClaimProvider';

export interface ILossLocationProps {

}

const LossLocation = (props: INewClaimStepProps) => {
    var context = useContext(NewClaimContext);

    const [mainForm] = Form.useForm();
    const [locations, setLocations] = useState<ILossLocations[]>([])
    const [showButtons, setShowButtons] = useState(false);

    const lossLocationGridHeader: ColumnsType<ILossLocations> = [
        {
            title: 'Client Name', key: 'clientName', dataIndex: 'clientName'
        },
        {
            title: 'Status', key: 'status', dataIndex: 'status', render: (text: string, rec: ILossLocations, idx: number) => {
                return (
                    <div className="status">
                        <Icon path={rec.status === 0 ? mdiCloseCircle : mdiCheckCircle} size="23px" className={'status-icon ' + (rec.status === 0 ? 'cancelled-icon' : 'active-icon')} />
                        <span>{rec.status === 0 ? 'Cancelled' : 'Active'}</span>
                    </div>
                )
            }
        },
        {
            title: 'Location Number', key: 'locationNumber', dataIndex: 'locationNumber', align: 'center'
        },
        {
            title: 'Location Address', key: 'street', dataIndex: ['address', 'street']
        },
        {
            title: 'City', key: 'city', dataIndex: ['address', 'city']
        },
        {
            title: 'Province', key: 'province', dataIndex: ['address', 'province']
        },
        {
            title: 'Postal Code', key: 'postal', dataIndex: ['address', 'postal']
        }
    ];

    const selectLocation = (location: ILossLocations) => {
        // console.log(location);
        context.setLocation(location);
        setShowButtons(true);
    }

    const formSubmit = (values: any) => {
        console.log(values)
    }

    const goBack = (e: React.MouseEvent<HTMLElement>) => {
        if (props.lastStep) {
            props.lastStep();
        }
    }

    useEffect(() => {
        // TODO: Query the backend

        var fakeData = [];
        var numberOfRec = faker.random.number({ min: 20, max: 100 });
        for (var i = 1; i <= numberOfRec; i++) {
            fakeData.push({
                key: i, // This is the (DOM) row key
                clientName: context.policy?.insured,
                status: faker.random.number({min: 0, max: 1}), //faker.random.arrayElement(['Active', 'Cancelled']),
                locationNumber: faker.random.number({ min: 1, max: 20 }),
                address: {
                    street: faker.address.streetAddress(),
                    city: faker.address.city(),
                    provCode: faker.address.stateAbbr(),
                    postal: faker.address.zipCode(),
                }
            })
        }

        setLocations(fakeData);
    }, [context.policy])

    return (
        <Fragment>
            <Space direction="vertical" size="middle">
                <div>Select loss location to get the list locations and their status lorem ipsum donar text placeholder.</div>

                <Form form={mainForm} onFinish={formSubmit}>
                    <Table className="locations"
                        size="small"
                        dataSource={locations}
                        columns={lossLocationGridHeader}
                        pagination={{
                            position: ['bottomLeft'],
                            showTotal: (total: number, range: [number, number]) => {
                                return (<span>{range[0]}-{range[1]} of {total} items</span>);
                            }
                        }}
                        rowSelection={{ type: 'radio', onChange: (key, row) => { console.log(key, row); selectLocation(row[0]); }}}
                    ></Table>
                    {showButtons ?
                    <Form.Item>
                        <Space direction="horizontal" size="small">
                            <Button type="default" htmlType="button" value="back" name="backward" onClick={goBack}>Back</Button>
                            <Button type="primary" htmlType="submit" value="next" name="forward">Continue</Button>
                        </Space>
                    </Form.Item>
                    : null}

                </Form>

            </Space>
        </Fragment>
    );
}

export default LossLocation;